package com.microsoft.appcenter.appium;

import org.junit.rules.TestWatcher;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.URL;
import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * Factory for creating Watcher and Enhanced drivers
 */
public class Factory {
    private Factory() {
    }

    private static final Provider provider = createProvider();

    private static Provider createProvider() {
        Iterator<Provider> service = ServiceLoader.load(Provider.class).iterator();
        if(service.hasNext()) {
            return service.next();
        }
        return new LocalProvider();
    }

    /**
     * Create enhanced android driver, use in place of {@link io.appium.java_client.android.AndroidDriver#AndroidDriver(URL, Capabilities)}
     *
     * @param url                 url of the server
     * @param desiredCapabilities desired capabilities for the session
     * @param <T>                 the required type of class which implement {@link org.openqa.selenium.WebElement}.
     *                            Instances of the defined type will be returned via findElement* and findElements*.
     *                            Warning (!!!). Allowed types:
     *                            {@link org.openqa.selenium.WebElement}
     *                            {@link org.openqa.selenium.remote.RemoteWebElement}
     *                            {@link io.appium.java_client.MobileElement}
     *                            {@link io.appium.java_client.android.AndroidElement}
     * @return enhanced Android driver
     */
    public static <T extends WebElement> EnhancedAndroidDriver<T> createAndroidDriver(URL url, DesiredCapabilities desiredCapabilities) {
        return provider.createAndroidDriver(url, desiredCapabilities);
    }

    /**
     * Create enhanced iOS driver
     * @param url url of the server
     * @param capabilities desired capabilities for the session
     * @param <T>                 the required type of class which implement {@link org.openqa.selenium.WebElement}.
     *                            Instances of the defined type will be returned via findElement* and findElements*.
     *                            Warning (!!!). Allowed types:
     *                            {@link org.openqa.selenium.WebElement}
     *                            {@link org.openqa.selenium.remote.RemoteWebElement}
     *                            {@link io.appium.java_client.MobileElement}
     *                            {@link io.appium.java_client.android.AndroidElement}
     * @return enhanced iOS driver
     */
    public static <T extends WebElement> EnhancedIOSDriver<T> createIOSDriver(URL url, DesiredCapabilities capabilities) {
        return provider.createIOSDriver(url, capabilities);
    }

    /**
     * Create watcher for JUnit
     * @return a watcher for use with JUnit
     */
    public static TestWatcher createWatcher() {
        return provider.createWatcher();
    }

}
