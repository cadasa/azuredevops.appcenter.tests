package com.microsoft.appcenter.appium;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public interface Device {

	public void label(String string) throws MalformedURLException;
	public void quit();
	public void clickElement(By Locator) throws InterruptedException;
	public void enterText(By Locator, String aText) throws InterruptedException;
	public WebElement wait(By Locator);
	public void setContext();
	public ElementLocator getLocator();
	public void acceptAlert();
	public boolean isElementPresent(By Locator);
	public void clickElementByIndex(By Locator, int index);
	public void pressEnter(String aKeyToPress);
	public WebElement wait(By Locator, int aTimeout);
	public void clickElementByCenterCoordinates(By aLocator);
	public String getPageSource();
	public String getCurrentActivity();
	public void selectBookingDate(int aDay, String aMonth, int aHour, int aMin) throws InterruptedException;
	public void selectReservationDate(int aYear, String aMonth, int aDay, int aHour, int aMin) throws InterruptedException;
	
}
