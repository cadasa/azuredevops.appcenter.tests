package com.microsoft.appcenter.appium;

import org.openqa.selenium.By;

public interface ElementLocator {
	public By getMenuLocator();
	public By getLoginURLButtonLocator();
	public By getLoginURLElementLocator();
	public By getLoginButtonLocator();
	public By getUserNameLocator();
	public By getPasswordLocator();
	public By getScanQRCodeLocator();
	public By getQRCodeStatusLocator();
	public By getModuleHomeButton(String aModuleName);
	public By getListRecord(String aListItemText);
	public By getLoadedStaetOfCAD();
	public By getLoadedStaetOfCADInReservationModule();
	public By getLocatorByText(String aLocatorText);
	public By getButtonLocatorByText(String aButtonText);
	public By getSearchBar();
	public By getCADToogleButton();
	public By getRegularFiltersButton();
	public By getLocatorByTextEqual(String aText);
	public By getFilterField(String aFieldName);
	public By getNonSelectedFilterField(String aFieldName);
}

