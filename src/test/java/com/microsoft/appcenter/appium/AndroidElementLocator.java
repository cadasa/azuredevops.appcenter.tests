package com.microsoft.appcenter.appium;

import org.openqa.selenium.By;

public class AndroidElementLocator implements ElementLocator {

	@Override
	public By getMenuLocator() {
		return By.xpath("//*[contains(@text,\"More\")]");
	}

	@Override
	public By getLoginURLButtonLocator() {
		return By.className("android.widget.Button");
	}

	@Override
	public By getLoginURLElementLocator() {
		return By.className("android.widget.EditText");
	}

	@Override
	public By getLoginButtonLocator() {
		return By.className("android.widget.Button");
	}
	
	@Override
	public By getUserNameLocator() {
		return By.xpath("//*[@resource-id='j_username']");
	}

	@Override
	public By getPasswordLocator() {
		return By.xpath("//*[@resource-id='j_password']");
	}

	@Override
	public By getScanQRCodeLocator() {
		return By.xpath("//*[@text='Scan QR code']");
	}

	@Override
	public By getQRCodeStatusLocator() {
		return By.xpath("//*[@text='Place a barcode inside the viewfinder rectangle to scan it.']");
		
	}
	
	@Override
	public By getSearchBar() {
		return By.xpath("//*[contains(@resource-id,'mat-input')]");
	}

	//For CAD Viewer Test
	
	@Override
	public By getModuleHomeButton(String aModuleName) {
		return getButtonLocatorByText(aModuleName);
	}
	
	@Override
	public By getLocatorByText(String aText) {
		return By.xpath("//*[contains(@text,\""+aText+"\")]");
	}
	
	@Override
	public By getLocatorByTextEqual(String aText) {
		return By.xpath("//*[@text='"+aText+"']");
	}
	
	@Override
	public By getListRecord(String aListItemText) {
		return getLocatorByText(aListItemText);	
	}
	
	@Override
	public By getLoadedStaetOfCAD() {
		return By.xpath("(//android.widget.Image[@index='0'])[2]");
	}
	
	@Override
	public By getButtonLocatorByText(String aButtonText) {
		return By.xpath("//android.widget.Button[contains(@text,\""+aButtonText+"\")]");
	}

	@Override
	public By getCADToogleButton() {
		return By.xpath("(//*[contains(@resource-id,'mat-slide-toggle')])[1]");
	}

	@Override
	public By getLoadedStaetOfCADInReservationModule() {
		return By.xpath("//android.view.View[contains(@text,'m')]");
	}

	@Override
	public By getRegularFiltersButton() {
		return By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[3]//android.widget.Button[@index='0']");
	}

	@Override
	public By getFilterField(String aFieldName) {
		return getLocatorByText(aFieldName);
	}

	@Override
	public By getNonSelectedFilterField(String aFieldName) {
		return getLocatorByText(aFieldName);
	}
}
