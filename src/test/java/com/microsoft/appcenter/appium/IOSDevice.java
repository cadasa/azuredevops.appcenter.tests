package com.microsoft.appcenter.appium;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;

public class IOSDevice implements Device {
	
	private EnhancedIOSDriver<IOSElement> driver;
	private WebDriverWait wait;
	private IOSElementLocator locator = new IOSElementLocator();
	
	public IOSDevice() throws MalformedURLException {
		//driver = Factory.createIOSDriver(new URL("http://192.168.1.166:4723"), getDeviceCapabilities());
		
		driver = Factory.createIOSDriver(new URL("http://localhost:8001"), getDeviceCapabilities());
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public IOSDevice(int aPort) throws MalformedURLException {
		driver = Factory.createIOSDriver(new URL("http://192.168.155.101:4545/wd/hub"), getDeviceCapabilities());
		
		//driver = Factory.createIOSDriver(new URL("http://localhost:"+aPort), getDeviceCapabilities());
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}


	public DesiredCapabilities getDeviceCapabilities() {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("fullReset", false);
		capabilities.setCapability("noReset", true);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("xcodeSigningId", "iPhone Developer");
        capabilities.setCapability("xcodeOrgId", "E79K52TU64");
        capabilities.setCapability("autoAcceptAlerts",true);
        capabilities.setCapability("bundleId","com.planonsoftware.mobileplatform");
        capabilities.setCapability("udid", "ADCCBBF7-5523-46D0-AD8B-BBAF4354CBA5"); //For simulator
		capabilities.setCapability("deviceName", "iPhone 13");//For simulator
		capabilities.setCapability("platformVersion", "16.1");
		//capabilities.setCapability("udid", "00008110-001610E80286401E");
		//capabilities.setCapability("deviceName", "Chiranjeevi's iPhone");
		//capabilities.setCapability(MobileCapabilityType.APP, "/Users/admin/Downloads/PlanonLive_Dev.ipa");
		
		return capabilities;
	}

	@Override
	public void label(String label) throws MalformedURLException {
		driver.label(label);
	}

	@Override
	public void quit() {
		driver.quit();
	}


	@Override
	public void clickElement(By aLocator) {
		try {
			new TouchAction(driver)
	        .tap(tapOptions().withElement(element(wait(aLocator)))).perform();
			
		}catch(StaleElementReferenceException e) {
			new TouchAction(driver)
	        .tap(tapOptions().withElement(element(wait(aLocator)))).perform();
		}
	}
	
	@Override
	public void clickElementByCenterCoordinates(By aLocator) {
		try {
			Point location = wait(aLocator).getLocation();
			Dimension d = wait(aLocator).getSize();
			int x = location.getX()+ (d.getWidth()/2);
			int y = location.getY()+ (d.getHeight()/2);
			new TouchAction(driver).tap(PointOption.point(x,y)).release().perform(); 
		}catch(StaleElementReferenceException e) {
			Point location = wait(aLocator).getLocation();
			Dimension d = wait(aLocator).getSize();
			int x = location.getX()+ (d.getWidth()/2);
			int y = location.getY()+ (d.getHeight()/2);
			new TouchAction(driver).tap(PointOption.point(x,y)).release().perform(); 
		}
	}

	@Override
	public void enterText(By aLocator, String aText) throws InterruptedException {
		try {
			wait(aLocator);
			clickElement(aLocator);
			wait(aLocator).clear();
			wait(aLocator).sendKeys(aText);
		}catch(StaleElementReferenceException e) {
			Thread.sleep(2000);
			clickElement(aLocator);
			wait(aLocator).clear();
			wait(aLocator).sendKeys(aText);
		}
	}


	@Override
	public WebElement wait(By aLocator) {
		return wait(aLocator, 30);
	}

	@Override
	public void setContext() {
		driver.context("NATIVE_APP");
	}


	@Override
	public ElementLocator getLocator() {
		return locator;
	}


	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		
	}


	@Override
	public boolean isElementPresent(By aLocator) {
		return wait(aLocator).isDisplayed();
	}


	@Override
	public void clickElementByIndex(By Locator, int index) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void pressEnter(String aKeyToPress) {
		driver.hideKeyboard(aKeyToPress);
	}


	@Override
	public WebElement wait(By Locator, int aTimeout) {
		WebDriverWait wait = new WebDriverWait(driver, aTimeout);
		return (IOSElement) wait.until(ExpectedConditions.presenceOfElementLocated(Locator));
	}


	@Override
	public String getPageSource() {
		return driver.getPageSource();
	}


	@Override
	public String getCurrentActivity() {
		try {
		if(isElementPresent(By.xpath("//XCUIElementTypeOther[@name=\"Planon mobile\"]"))) {
			return "MainActivity";
		}
		}catch(TimeoutException e) {
			return "chromium"; //Return chromium if app runs at background to match android behavior
		}
		return "chromium"; //Return chromium if app runs at background to match android behavior
	}


	@Override
	public void selectBookingDate(int aDay, String aMonth, int aHour, int aMin) throws InterruptedException {

		// Selection of Day And Month
		String aExpDayAndMonth = String.valueOf(aDay) + " " + aMonth;
		System.out.println("First one is finished ");
		for (int i = 1; i < 32; i++) {
		    WebElement ele = driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]/XCUIElementTypeOther[5]/XCUIElementTypeOther[@visible='true']/following-sibling::XCUIElementTypeOther"));
		    scrollToElement(ele, "down");
			Thread.sleep(500);
			String selectedDayAndMonth = driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]/XCUIElementTypeOther[5]/XCUIElementTypeOther[@visible='true']")).getText();
			System.out.println("Selected day and month is :" + selectedDayAndMonth);
			if (selectedDayAndMonth.contains(aExpDayAndMonth)) {
				break;
			}
		}

		// Selection of Hour
		int currentHour = Integer.parseInt(driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]/XCUIElementTypeOther[6]/XCUIElementTypeOther[@visible='true']")).getText());
		System.out.println("Captured current hour is:" + currentHour);
		int actualHourDifference = currentHour - aHour;
		actualHourDifference = (actualHourDifference < 0) ? -actualHourDifference : actualHourDifference;
		String direction;
		for (int i = 0; i < actualHourDifference; i++) {
			if (currentHour == aHour) {
				break;
			}
			if (aHour > currentHour) {
				currentHour++;
				direction = "down";
			} else {
				currentHour--;
				direction = "up";
			}
			System.out.println("Hour is:" + currentHour);
			WebElement ele = driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]/XCUIElementTypeOther[6]//XCUIElementTypeOther[contains(@name,\""
					+ ((currentHour < 10) ? "0" : "") + currentHour + "\")]"));
			scrollToElement(ele, direction);

			Thread.sleep(500);
		}

		// Selection of Min
		int currentMin = Integer.parseInt(driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]/XCUIElementTypeOther[7]/XCUIElementTypeOther[@visible='true']")).getText());
		System.out.println("Captured current min is:" + currentMin);
		int actualMinDifference = currentMin - aMin;
		actualMinDifference = (actualMinDifference < 0) ? -actualMinDifference : actualMinDifference;
		for (int i = 0; i < actualMinDifference / 5; i++) {
			if (currentMin == aMin) {
				break;
			}
			if (aMin > currentMin) {
				currentMin = currentMin + 5;
				direction = "down";
			} else {
				currentMin = currentMin - 5;
				direction = "up";
			}
			System.out.println("current min is: " + currentMin);
			WebElement ele = driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]/XCUIElementTypeOther[7]//XCUIElementTypeOther[contains(@name,\""
					+ ((currentMin < 10) ? "0" : "") + currentMin + "\")]"));
			scrollToElement(ele, direction);
			Thread.sleep(500);
		}

		driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Set\"]")).click();
	}

	@Override
	public void selectReservationDate(int aYear, String aMonth, int aDay, int aHour, int aMin)
			throws InterruptedException {
		driver.findElement(By.xpath("//*[contains(@name,\"Create reservation from\")][1]")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//*[contains(@name,\"Choose month and year\")][1]")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]//XCUIElementTypeOther[@name=\""+aYear+"\"]")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]//XCUIElementTypeOther[contains(@name,\"" + aMonth + "\")]"))
				.click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]//XCUIElementTypeOther[contains(@name,\"" + aDay + "\")]"))
		.click();
		Thread.sleep(500);
		driver.findElements(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]//XCUIElementTypeTextField")).get(0).clear();
		driver.findElements(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]//XCUIElementTypeTextField")).get(0).sendKeys(String.valueOf(aHour));
		Thread.sleep(500);
		driver.findElements(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]//XCUIElementTypeTextField")).get(1).clear();
		driver.findElements(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]//XCUIElementTypeTextField")).get(1).sendKeys(String.valueOf(aMin));
		Thread.sleep(500);
		driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"web dialog\"]/XCUIElementTypeButton[last()]")).click();
	}
	
	
	public void scrollToElement(WebElement aWebElement, String direction) {
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("element", ((RemoteWebElement)aWebElement).getId());
		scrollObject.put("direction", direction );
		driver.executeScript("mobile: scroll",scrollObject);
	}

}
