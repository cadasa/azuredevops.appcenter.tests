package com.microsoft.appcenter.appium;

import org.openqa.selenium.By;

public class IOSElementLocator implements ElementLocator {

	@Override
	public By getMenuLocator() {
		return getButtonLocatorByText("More");
	}

	@Override
	public By getLoginURLButtonLocator() {
		return getButtonLocatorByText("Log in");
	}

	@Override
	public By getLoginURLElementLocator() {
		return By.xpath("//XCUIElementTypeTextField[@type=\"XCUIElementTypeTextField\"]");
	}

	@Override
	public By getLoginButtonLocator() {
		return getButtonLocatorByText("Login");
	}

	@Override
	public By getUserNameLocator() {
		return By.xpath("//XCUIElementTypeOther[@name=\"Planon Login\"]/XCUIElementTypeOther[2]/XCUIElementTypeTextField");
	}

	@Override
	public By getPasswordLocator() {
		return By.xpath("//XCUIElementTypeOther[@name=\"Planon Login\"]/XCUIElementTypeOther[3]/XCUIElementTypeSecureTextField");
	}
	
	@Override
	public By getScanQRCodeLocator() {
		return By.xpath("//*[@text='Scan QR code']");
	}

	@Override
	public By getQRCodeStatusLocator() {
		return By.xpath("//*[@text='Place a barcode inside the viewfinder rectangle to scan it.']");
		
	}

	@Override
	public By getModuleHomeButton(String aModuleName) {
		return getButtonLocatorByText(aModuleName);
	}

	@Override
	public By getListRecord(String aListItemText) {
		return getLocatorByText(aListItemText);	
	}

	@Override
	public By getLoadedStaetOfCAD() {
		return By.xpath("//XCUIElementTypeImage[12]");
	}

	@Override
	public By getLocatorByText(String aText) {
		return By.xpath("//*[contains(@name,\""+aText+"\")][1]");
	}

	@Override
	public By getSearchBar() {
		return By.xpath("//XCUIElementTypeSearchField");
	}

	@Override
	public By getButtonLocatorByText(String aButtonText) {
		return By.xpath("(//XCUIElementTypeButton[contains(@name, '"+aButtonText+"')])[1]");
	}

	@Override
	public By getCADToogleButton() {
		return By.xpath("//XCUIElementTypeOther[@name=\"Planon mobile\"]/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText");
	}

	@Override
	public By getLoadedStaetOfCADInReservationModule() {
		return By.xpath("//XCUIElementTypeImage[12]");
	}

	@Override
	public By getLocatorByTextEqual(String aText) {
		return By.xpath("//*[@name='"+aText+"'][1]");
	}

	@Override
	public By getRegularFiltersButton() {
		return By.xpath("(//XCUIElementTypeButton[contains(@name, ' ')])[1]");
	}

	@Override
	public By getFilterField(String aFieldName) {
		return By.xpath("//*[contains(@name,\""+aFieldName+"\")][1]//preceding-sibling::XCUIElementTypeStaticText[3]");
	}

	@Override
	public By getNonSelectedFilterField(String aFieldName) {
		return By.xpath("//*[contains(@name,\""+aFieldName+"\")][1]//preceding-sibling::XCUIElementTypeTextField");
	}
}
