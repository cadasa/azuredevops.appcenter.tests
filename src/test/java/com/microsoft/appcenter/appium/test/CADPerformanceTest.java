// Planon Enterprise Edition Source file: CADPerformanceTest.java
// Copyright Planon 1997-2023. All Rights Reserved.
package com.microsoft.appcenter.appium.test;

import com.microsoft.appcenter.appium.*;

import org.apache.commons.lang.time.*;
import org.junit.*;
import org.junit.rules.*;
import org.junit.runners.*;

import java.net.*;

/**
 * DOCUMENT ME!
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CADPerformanceTest extends BaseRealDeviceTest
{
  //~ Instance Variables ---------------------------------------------------------------------------

  @Rule public TestWatcher watcher = Factory.createWatcher();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new CADPerformanceTest object.
   *
   * @throws MalformedURLException DOCUMENT ME!
   */
  public CADPerformanceTest() throws MalformedURLException
  {
    super();
    // TODO Auto-generated constructor stub
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testAPraisHiveBookingModuleFlexWorkSpaceCADViewerPerformanceTest() throws InterruptedException
  {
    bookingCADPerformanceTest("Flexible workspaces", "PLANON PARIS_HIVE");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testBParisHiveBookingModuleSpaceUnitCADViewerPerformanceTest() throws InterruptedException
  {
    bookingCADPerformanceTest("Space units", "PLANON PARIS_HIVE");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testCParisHiveReservationModuleFlexWorkSpaceCADViewerPerformanceTest() throws InterruptedException
  {
    reservationCADPerformanceTest("Book Desk", "PLANON PARIS_HIVE");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testDParisHiveReservationModuleSpaceUnitCADViewerPerformanceTest() throws InterruptedException
  {
    reservationCADPerformanceTest("Book Room", "PLANON PARIS_HIVE");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testEHenkelBookingModuleFlexWorkSpaceCADViewerPerformanceTest() throws InterruptedException
  {
    bookingCADPerformanceTest("Flexible workspaces", "PROPERTY_HENKEL");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testFHenkelBookingModuleSpaceUnitCADViewerPerformanceTest() throws InterruptedException
  {
    bookingCADPerformanceTest("Space units", "PROPERTY_HENKEL");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testGHenkelReservationModuleFlexWorkSpaceCADViewerPerformanceTest() throws InterruptedException
  {
    reservationCADPerformanceTest("Book Desk", "PROPERTY_HENKEL");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testHHenkelReservationModuleSpaceUnitCADViewerPerformanceTest() throws InterruptedException
  {
    reservationCADPerformanceTest("Book Room", "PROPERTY_HENKEL");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testIBrainTreeBookingModuleFlexWorkSpaceCADViewerPerformanceTest() throws InterruptedException
  {
    bookingCADPerformanceTest("Flexible workspaces", "BRAINTREE_HILL");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testJBrainTreeBookingModuleSpaceUnitCADViewerPerformanceTest() throws InterruptedException
  {
    bookingCADPerformanceTest("Space units", "BRAINTREE_HILL");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testKBrainTreeReservationModuleFlexWorkSpaceCADViewerPerformanceTest() throws InterruptedException
  {
    reservationCADPerformanceTest("Book Desk", "BRAINTREE_HILL");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testLBrainTreeReservationModuleSpaceUnitCADViewerPerformanceTest() throws InterruptedException
  {
    reservationCADPerformanceTest("Book Room", "BRAINTREE_HILL");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testMColumbusSquareBookingModuleFlexWorkSpaceCADViewerPerformanceTest() throws InterruptedException
  {
    bookingCADPerformanceTest("Flexible workspaces", "Columbus Square");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testNColumbusSquareBookingModuleSpaceUnitCADViewerPerformanceTest() throws InterruptedException
  {
    bookingCADPerformanceTest("Space units", "Columbus Square");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testOColumbusSquareReservationModuleFlexWorkSpaceCADViewerPerformanceTest() throws InterruptedException
  {
    reservationCADPerformanceTest("Book Desk", "Columbus Square");
  }


  /**
   * DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Test public void testPColumbusSquareReservationModuleSpaceUnitCADViewerPerformanceTest() throws InterruptedException
  {
    reservationCADPerformanceTest("Book Room", "Columbus Square");
  }


  /**
   * DOCUMENT ME!
   *
   * @param  reservationType DOCUMENT ME!
   * @param  aProperty       DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  public void reservationCADPerformanceTest(String reservationType, String aProperty) throws InterruptedException
  {
    StopWatch loadTime = new StopWatch();

    login(appURL, appUserName, appPassword);

    // Clicking on booking module
    testDevice.clickElement(testDevice.getLocator().getModuleHomeButton("Reservations"));

    Thread.sleep(2000);

    // Clicking on flexible workspace button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText(reservationType));

    Thread.sleep(2000);

    // Clicking on regular filter button
    testDevice.clickElement(testDevice.getLocator().getRegularFiltersButton());

    Thread.sleep(2000);

    // Clicking on property filter
    testDevice.clickElement(testDevice.getLocator().getFilterField("Property"));

    Thread.sleep(2000);

    // Entering search string as property name
    testDevice.enterText(testDevice.getLocator().getSearchBar(), aProperty.substring(0, aProperty.length() - 2));
    testDevice.pressEnter("search");

    Thread.sleep(2000);

    // Clicking on property record
    testDevice.clickElement(testDevice.getLocator().getLocatorByText(aProperty));

    Thread.sleep(2000);

    // Clicking on floors filter
    testDevice.clickElement(testDevice.getLocator().getNonSelectedFilterField("Floor"));

    Thread.sleep(2000);

    // Clicking on First floor
    testDevice.clickElement(testDevice.getLocator().getLocatorByText("First"));

    Thread.sleep(2000);

    // Clicking on flexible workspace button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Apply"));

    Thread.sleep(2000);

    // Clicking on date field
    testDevice.selectReservationDate(2023, "May", 22, 10, 30);

    Thread.sleep(2000);

    // Clicking on Second floor
    testDevice.clickElementByCenterCoordinates(testDevice.getLocator().getCADToogleButton());

    // Starting timer
    loadTime.start();

    // Wait for CAD to load
    testDevice.wait(testDevice.getLocator().getLoadedStaetOfCADInReservationModule());
    loadTime.stop();

    // Console out the result
    long pageLoadTime_ms = loadTime.getTime();
    System.out.println(deviceName + " - Reservation Module - " + reservationType + " - " + aProperty
      + " - CAD First Time Load Duration : " + pageLoadTime_ms + " milliseconds");
    loadTime.reset();

    // Clicking on CAD toogle button
    testDevice.clickElementByCenterCoordinates(testDevice.getLocator().getCADToogleButton());

    Thread.sleep(2000);

    // Clicking on back button to got to home page
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Back"));

    Thread.sleep(2000);

    // Clicking on flexible workspace button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText(reservationType));

    Thread.sleep(2000);

    // Clicking on date field
    testDevice.selectReservationDate(2023, "May", 22, 10, 30);

    Thread.sleep(2000);

    // Clicking on CAD toogle button
    testDevice.clickElementByCenterCoordinates(testDevice.getLocator().getCADToogleButton());

    // Starting timer
    loadTime.start();

    // Wait for CAD to load
    testDevice.wait(testDevice.getLocator().getLoadedStaetOfCADInReservationModule());
    loadTime.stop();

    // Console out the result
    pageLoadTime_ms = loadTime.getTime();
    System.out.println(deviceName + " - Reservation Module - " + reservationType + " - " + aProperty
      + " - CAD Second Time Load Duration : " + pageLoadTime_ms + " milliseconds");
    loadTime.reset();

    // Clicking on CAD toogle button
    testDevice.clickElementByCenterCoordinates(testDevice.getLocator().getCADToogleButton());

    Thread.sleep(2000);

    // Clicking on Floors tab
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("First"));

    Thread.sleep(2000);

    // Clicking on Second floor
    testDevice.clickElement(testDevice.getLocator().getLocatorByText("Second"));

    Thread.sleep(2000);

    // Clicking on date field
    testDevice.selectReservationDate(2023, "May", 22, 10, 30);

    Thread.sleep(2000);

    // Clicking on CAD toogle button
    testDevice.clickElementByCenterCoordinates(testDevice.getLocator().getCADToogleButton());

    // Starting timer
    loadTime.start();

    // Wait for CAD to load
    testDevice.wait(testDevice.getLocator().getLoadedStaetOfCADInReservationModule());
    loadTime.stop();

    // Console out the result
    pageLoadTime_ms = loadTime.getTime();
    System.out.println(deviceName + " - Reservation Module - " + reservationType + " - " + aProperty
      + " - CAD Floor Change - Second floor Load Duration : " + pageLoadTime_ms + " milliseconds");
    loadTime.reset();

    Thread.sleep(2000);

    // Clicking on CAD toogle button
    testDevice.clickElementByCenterCoordinates(testDevice.getLocator().getCADToogleButton());

    Thread.sleep(2000); // To save setting at back office.
  }


  /**
   * DOCUMENT ME!
   *
   * @param  reservationType DOCUMENT ME!
   * @param  aProperty       DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  public void bookingCADPerformanceTest(String reservationType, String aProperty) throws InterruptedException
  {
    StopWatch loadTime = new StopWatch();
    StopWatch PureCADloadTime = new StopWatch();

    login(appURL, appUserName, appPassword);

    // Clicking on booking module
    testDevice.clickElement(testDevice.getLocator().getModuleHomeButton("Booking"));

    Thread.sleep(2000);

    // Clicking on flexible workspace button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText(reservationType));

    Thread.sleep(2000);

    // Clicking on all floors to select property
    testDevice.clickElement(testDevice.getLocator().getLocatorByText("Property"));

    Thread.sleep(2000);

    // Entering search string as property name
    testDevice.enterText(testDevice.getLocator().getSearchBar(), aProperty.substring(0, aProperty.length() - 2));
    testDevice.pressEnter("search");

    Thread.sleep(2000);

    // Clicking on property record
    testDevice.clickElement(testDevice.getLocator().getLocatorByTextEqual(aProperty));

    Thread.sleep(2000);

    // Clicking on First floor record
    testDevice.clickElement(testDevice.getLocator().getLocatorByText("First"));

    Thread.sleep(2000);

    // Clicking on date field
    testDevice.clickElement(testDevice.getLocator().getLocatorByText("When"));

    Thread.sleep(2000);

    // Clicking on date field
    testDevice.selectBookingDate(22, "May", 10, 30);

    Thread.sleep(2000);

    // Clicking on show results button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Show results"));

    // Starting timer
    loadTime.start();

    // Clicking on Map button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Map"));
    PureCADloadTime.start();

    // Wait for CAD to load
    testDevice.wait(testDevice.getLocator().getLoadedStaetOfCAD());
    PureCADloadTime.stop();
    loadTime.stop();

    // Console out the pure CAD diagram load time
    System.out.println(deviceName + " - Booking Module - " + reservationType + " - " + aProperty
      + " - CAD First Time Load Duration (From MAP to CAD) : " + PureCADloadTime.getTime() + " milliseconds");
    PureCADloadTime.reset();

    // Console out the result
    long pageLoadTime_ms = loadTime.getTime();
    System.out.println(deviceName + " - Booking Module - " + reservationType + " - " + aProperty
      + " - CAD First Time Load Duration : " + pageLoadTime_ms + " milliseconds");
    loadTime.reset();

    // Clicking on back button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Back"));

    Thread.sleep(2000);

    // Clicking on show results button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Show results"));

    // Starting timer
    loadTime.start();

    // Clicking on Map button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Map"));
    PureCADloadTime.start();

    // Wait for CAD to load
    testDevice.wait(testDevice.getLocator().getLoadedStaetOfCAD());
    PureCADloadTime.stop();
    loadTime.stop();

    // Console out the time from MAP to CAD
    System.out.println(deviceName + " - Booking Module - " + reservationType + " - " + aProperty
      + " - CAD Second Time Load Duration (From MAP to CAD) : " + PureCADloadTime.getTime() + " milliseconds");
    PureCADloadTime.reset();

    // Console out the result
    pageLoadTime_ms = loadTime.getTime();
    System.out.println(deviceName + " - Booking Module - " + reservationType + " - " + aProperty
      + " - CAD Second Time Load Duration : " + pageLoadTime_ms + " milliseconds");
    loadTime.reset();

    // Clicking on back button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Back"));

    Thread.sleep(2000);

    // Clicking on Floors tab
    testDevice.clickElement(testDevice.getLocator().getLocatorByText("Floor"));

    Thread.sleep(2000);

    // Clicking on Second floor
    testDevice.clickElement(testDevice.getLocator().getLocatorByText("Second"));

    Thread.sleep(2000);

    // Clicking on show results button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Show results"));

    // Starting timer
    loadTime.start();

    // Clicking on Map button
    testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("Map"));
    PureCADloadTime.start();

    // Wait for CAD to load
    testDevice.wait(testDevice.getLocator().getLoadedStaetOfCAD());
    PureCADloadTime.stop();
    loadTime.stop();

    // Console out the load time from MAP to CAD
    System.out.println(deviceName + " - Booking Module - " + reservationType + " - " + aProperty
      + " - CAD Floor Change - Second floor Load Duration (From MAP to CAD) : " + PureCADloadTime.getTime() + " milliseconds");
    PureCADloadTime.reset();

    // Console out the result
    pageLoadTime_ms = loadTime.getTime();
    System.out.println(deviceName + " - Booking Module - " + reservationType + " - " + aProperty
      + " - CAD Floor Change - Second floor Load Duration : " + pageLoadTime_ms + " milliseconds");
    loadTime.reset();
  }
}
