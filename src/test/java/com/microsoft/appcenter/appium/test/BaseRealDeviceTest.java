// Planon Enterprise Edition Source file: RealDeviceTest.java
// Copyright Planon 1997-2023. All Rights Reserved.
package com.microsoft.appcenter.appium.test;

import org.junit.*;
import org.junit.rules.*;
import org.openqa.selenium.*;

import com.microsoft.appcenter.appium.AppiumMock;
import com.microsoft.appcenter.appium.Device;
import com.microsoft.appcenter.appium.Factory;
import com.microsoft.appcenter.appium.RealDeviceTestFactory;

import java.io.*;
import java.net.*;

/**
 * BaseRealdeviceTest
 */
public class BaseRealDeviceTest
{
	public static final byte[] PNG = OutputType.BYTES
			.convertFromBase64Png("amZsanNmbGtkc2pmbGtzZGpmbGtkc2ZqbGtkc2ZqZGxza2ZqZHNsa2Zq");
	private static AppiumMock appiumMock;
	private boolean isLoginRequired = false;
	Device testDevice;
	RealDeviceTestFactory testFactory;
	String appURL;
	String appUserName;
	String appPassword;
	String deviceName;
	private static int validPortForRun;

	@Rule
	public TestWatcher watcher = Factory.createWatcher();

//private String appURL = "https://cadperftest2-prod.plnd.cloud";
//private String appUserName = "weasly";
//private String appPassword = "a";

	public BaseRealDeviceTest() throws MalformedURLException {

		appURL = System.getenv("APP_URL");
		appUserName = System.getenv("APP_USERNAME");
		appPassword = System.getenv("APP_PASSWORD");
		deviceName = System.getenv("XTC_DEVICE");
		testFactory = new RealDeviceTestFactory();
		validPortForRun = testFactory.getValidPort();

	}

	@BeforeClass
	public static void mockAppium() throws IOException {
		appiumMock = new AppiumMock();
		appiumMock.start(validPortForRun);
	}

	@AfterClass
	public static void stopAppium() throws IOException {
		appiumMock.stop();
	}

	@Before
	public void setUp() throws MalformedURLException, InterruptedException {

		testDevice = testFactory.getTestDevice(validPortForRun);
		testDevice.setContext();

	}

	@After
	public void TearDown() throws MalformedURLException {
		testDevice.label("Stopping App");
		testDevice.quit();
	}
	
	public Device getDevice() {
		return this.testDevice;
	}

	public void login(String aURL, String aUserName, String aPassword) throws InterruptedException {

		By menu = testDevice.getLocator().getMenuLocator();
		Thread.sleep(5000);

		try {

			testDevice.wait(menu, 15);
			isLoginRequired = false;

		} catch (Exception e) {

			// Because test might be the first one OR app got reset in a previous test
			isLoginRequired = true;
		}

		if (isLoginRequired) {
			//System.out.print("Page Source of URL page:"+testDevice.getPageSource());
			By urlElement = testDevice.getLocator().getLoginURLElementLocator();
			testDevice.wait(urlElement);

			testDevice.clickElement(urlElement);
			testDevice.enterText(urlElement, aURL);
			testDevice.pressEnter("go");

			By userName = testDevice.getLocator().getUserNameLocator();
			By password = testDevice.getLocator().getPasswordLocator();

			try {
			testDevice.wait(userName);

			testDevice.enterText(userName, aUserName);
			testDevice.enterText(password, aPassword);
			testDevice.pressEnter("go");
			}catch(Exception e) {
				//On IOS, login might have finished already using keystore credentials. So do not do anything
			}

			testDevice.wait(menu, 90);

		}

	}
}
