package com.microsoft.appcenter.appium.test;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import com.microsoft.appcenter.appium.Factory;

import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MobilePlatformRealDeviceTest extends BaseRealDeviceTest {
	public MobilePlatformRealDeviceTest() throws MalformedURLException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Rule
	public TestWatcher watcher = Factory.createWatcher();


	@Test
	public void testExternlPSSDeepLinks() throws InterruptedException {
		login(appURL, appUserName, appPassword);

		Thread.sleep(2000);

		// Clicking on more button
		testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("More"));

		Thread.sleep(2000);

		// Clicking on external services button
		testDevice.clickElement(testDevice.getLocator().getButtonLocatorByText("External services"));

		Thread.sleep(2000);
		
		// Verify that News links is available
		testDevice.isElementPresent(testDevice.getLocator().getLocatorByText("News"));

	}
}
