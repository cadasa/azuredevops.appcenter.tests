package com.microsoft.appcenter.appium;

import org.openqa.selenium.Capabilities;
import java.net.URL;

interface Provider {
    Watcher createWatcher();
    EnhancedAndroidDriver createAndroidDriver(URL remoteAddress, Capabilities desiredCapabilities);
    EnhancedIOSDriver createIOSDriver(URL remoteAddress, Capabilities desiredCapabilities);
}
