package com.microsoft.appcenter.appium;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.util.Random;

public class RealDeviceTestFactory {
	
	
	public Platform getPlatformType() {
		
		String envPlatform = System.getenv("XTC_PLATFORM");
        if (envPlatform == null) {
            throw new RuntimeException("The 'XTC_PLATFORM' environment variable is not set");
        } else if (envPlatform.equals("android")) {
            return Platform.ANDROID;
        } else if (envPlatform.equals("ios") || envPlatform.equals("ios-simulator")) {
            return Platform.IOS;
        } else {
            throw new RuntimeException("Platform not supported: " + envPlatform);
        }
	}
	
	public Device getTestDevice(int aPort) throws MalformedURLException {
		
		Platform platformType = getPlatformType();
		if(platformType == Platform.ANDROID) {
			
			return new AndroidDevice(aPort);
		} else if (platformType == Platform.IOS) {
			
			return new IOSDevice(aPort);
		}
		
		return null;
	}
	
	  /**
	   * Checks to see if a specific port is available.
	   *
	   * @param  port the port to check for availability
	   *
	   * @return true if port is available. false if port is not available
	   *
	   * @throws IllegalArgumentException DOCUMENT ME!
	   */
	  private static boolean available(int port)
	  {
	    if ((port < 8000) || (port > 9000))
	    {
	      throw new IllegalArgumentException("Invalid start port: " + port);
	    }

	    ServerSocket ss = null;
	    DatagramSocket ds = null;
	    try
	    {
	      ss = new ServerSocket(port);
	      ss.setReuseAddress(true);
	      ds = new DatagramSocket(port);
	      ds.setReuseAddress(true);
	      return true;
	    }
	    catch (IOException e)
	    {
	    }
	    finally
	    {
	      if (ds != null)
	      {
	        ds.close();
	      }

	      if (ss != null)
	      {
	        try
	        {
	          ss.close();
	        }
	        catch (IOException e)
	        {
	          /* should not be thrown */
	        }
	      }
	    }

	    return false;
	  }


	  /**
	   * Generate Random Number
	   *
	   * @param  min
	   * @param  max
	   *
	   * @return integer
	   *
	   * @throws IllegalArgumentException
	   */
	  private static int getRandomNumberInRange(int min, int max)
	  {
	    if (min >= max)
	    {
	      throw new IllegalArgumentException("max must be greater than min");
	    }

	    Random r = new Random();
	    return r.nextInt((max - min) + 1) + min;
	  }
	  
	  
	  public static int getValidPort() {
		    int randomPortNumber = getRandomNumberInRange(8000, 9000);
		    while (!available(randomPortNumber))
		    {
		      randomPortNumber = getRandomNumberInRange(8000, 9000);
		    }
		    
		    return randomPortNumber;
	  }  
}
