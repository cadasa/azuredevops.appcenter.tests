// Planon Enterprise Edition Source file: AndroidDevice.java
// Copyright Planon 1997-2023. All Rights Reserved.
package com.microsoft.appcenter.appium;

import io.appium.java_client.*;
import io.appium.java_client.android.*;
import io.appium.java_client.remote.*;
import static io.appium.java_client.touch.TapOptions.*;
import static io.appium.java_client.touch.WaitOptions.*;
import io.appium.java_client.touch.offset.*;
import static io.appium.java_client.touch.offset.ElementOption.*;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.*;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.support.ui.*;

import java.io.*;
import java.net.*;
import static java.time.Duration.*;
import java.util.concurrent.*;

/**
 * DOCUMENT ME!
 */
public class AndroidDevice implements Device
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private AndroidElementLocator locator = new AndroidElementLocator();

  private EnhancedAndroidDriver<AndroidElement> driver;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AndroidDevice object.
   *
   * @param  aPort DOCUMENT ME!
   *
   * @throws MalformedURLException DOCUMENT ME!
   */
  public AndroidDevice(int aPort) throws MalformedURLException
  {
    //driver = Factory.createAndroidDriver(new URL("http://192.168.155.133:4545/wd/hub"), getDeviceCapabilities());

    driver = Factory.createAndroidDriver(new URL("http://localhost:" + aPort), getDeviceCapabilities());
    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  private DesiredCapabilities getDeviceCapabilities()
  {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setCapability("automationName", "uiautomator2");
    capabilities.setCapability("appPackage", "com.planonsoftware.mobileplatform");
    capabilities.setCapability("appActivity", "com.planonsoftware.mobileplatform.MainActivity");
    capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
    capabilities.setCapability("appWaitPackage", "50000");
    capabilities.setCapability("appWaitDuration", 50000);
    capabilities.setCapability("newCommandTimeout", 100);
    capabilities.setCapability("fullReset", true);
    capabilities.setCapability("noReset", false);
    capabilities.setCapability("deviceName", "emulator-5554");
    File appDir = new File("app/Android/app-debug_3_1_4.apk");
    capabilities.setCapability(MobileCapabilityType.APP, appDir.getAbsolutePath());

    return capabilities;
  }


  /**
   * DOCUMENT ME!
   *
   * @param  label DOCUMENT ME!
   *
   * @throws MalformedURLException DOCUMENT ME!
   */
  @Override public void label(String label) throws MalformedURLException
  {
    driver.label(label);
  }


  /**
   * DOCUMENT ME!
   */
  @Override public void quit()
  {
    driver.quit();
  }


  /**
   * DOCUMENT ME!
   *
   * @param  Locator DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Override public void clickElement(By Locator) throws InterruptedException
  {
    try
    {
      new TouchAction(driver).tap(tapOptions().withElement(element(wait(Locator)))).waitAction(waitOptions(ofMillis(50))).perform();
    }
    catch (StaleElementReferenceException e)
    {
      new TouchAction(driver).tap(tapOptions().withElement(element(wait(Locator)))).waitAction(waitOptions(ofMillis(50))).perform();
    }
  }


  /**
   * DOCUMENT ME!
   *
   * @param Locator DOCUMENT ME!
   * @param index   DOCUMENT ME!
   */
  @Override public void clickElementByIndex(By Locator, int index)
  {
    new TouchAction(driver).tap(tapOptions().withElement(element(driver.findElements(Locator).get(index)))).waitAction(waitOptions(ofMillis(50))).perform();
  }


  /**
   * DOCUMENT ME!
   *
   * @param  Locator DOCUMENT ME!
   * @param  aText   DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Override public void enterText(By Locator, String aText) throws InterruptedException
  {
    try
    {
      wait(Locator);
      clickElement(Locator);
      wait(Locator).clear();
      wait(Locator).sendKeys(aText);
    }
    catch (StaleElementReferenceException e)
    {
      Thread.sleep(2000);
      clickElement(Locator);
      wait(Locator).clear();
      wait(Locator).sendKeys(aText);
    }
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aLocator DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  @Override public AndroidElement wait(By aLocator)
  {
    return wait(aLocator, 30);
  }


  /**
   * DOCUMENT ME!
   *
   * @param  Locator  DOCUMENT ME!
   * @param  aTimeout DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  @Override public AndroidElement wait(By Locator, int aTimeout)
  {
    WebDriverWait wait = new WebDriverWait(driver, aTimeout);
    return (AndroidElement) wait.until(ExpectedConditions.visibilityOfElementLocated(Locator));
  }


  /**
   * DOCUMENT ME!
   */
  @Override public void setContext()
  {
    driver.context("NATIVE_APP");
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  @Override public ElementLocator getLocator()
  {
    return locator;
  }


  /**
   * DOCUMENT ME!
   */
  @Override public void acceptAlert()
  {
    driver.switchTo().alert().accept();
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aLocator DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  @Override public boolean isElementPresent(By aLocator)
  {
    return wait(aLocator).isDisplayed();
  }


  /**
   * DOCUMENT ME!
   *
   * @param aKeyToPress DOCUMENT ME!
   */
  @Override public void pressEnter(String aKeyToPress)
  {
    // ignore argument as it is needed only for IOS device
    new Actions(driver).sendKeys(Keys.ENTER).perform();
  }


  /**
   * DOCUMENT ME!
   */
  public void clickDeviceBackButton()
  {
    driver.pressKeyCode(AndroidKeyCode.BACK);
  }


  /**
   * DOCUMENT ME!
   *
   * @param aLocator DOCUMENT ME!
   */
  @Override public void clickElementByCenterCoordinates(By aLocator)
  {
    try
    {
      Point location = wait(aLocator).getLocation();
      Dimension d = wait(aLocator).getSize();
      int x = location.getX() + (d.getWidth() / 2);
      int y = location.getY() + (d.getHeight() / 2);
      new TouchAction(driver).tap(PointOption.point(x, y)).release().perform();
    }
    catch (StaleElementReferenceException e)
    {
      Point location = wait(aLocator).getLocation();
      Dimension d = wait(aLocator).getSize();
      int x = location.getX() + (d.getWidth() / 2);
      int y = location.getY() + (d.getHeight() / 2);
      new TouchAction(driver).tap(PointOption.point(x, y)).release().perform();
    }
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  @Override public String getPageSource()
  {
    return driver.getPageSource();
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  @Override public String getCurrentActivity()
  {
    return driver.currentActivity();
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aDay   DOCUMENT ME!
   * @param  aMonth DOCUMENT ME!
   * @param  aHour  DOCUMENT ME!
   * @param  aMin   DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Override public void selectBookingDate(int aDay, String aMonth, int aHour, int aMin) throws InterruptedException
  {
    // Selection of Day And Month
    String aExpDayAndMonth = String.valueOf(aDay) + " " + aMonth;
    for (int i = 1; i < 32; i++)
    {
      driver.findElement(By.xpath("//android.widget.ListView[1]//android.view.View[@selected='true']/following-sibling::android.view.View")).click();
      Thread.sleep(500);
      String selectedDayAndMonth = driver.findElement(By.xpath("//android.widget.ListView[1]//android.view.View[@selected='true']")).getText();
      if (selectedDayAndMonth.contains(aExpDayAndMonth))
      {
        System.out.println("Selected day and month is :" + selectedDayAndMonth);
        break;
      }
    }

    // Selection of Hour
    int currentHour = Integer.parseInt(driver.findElement(By.xpath("//android.widget.ListView[2]//android.view.View[@selected='true']")).getText());
    int actualHourDifference = currentHour - aHour;
    actualHourDifference = (actualHourDifference < 0) ? -actualHourDifference : actualHourDifference;
    for (int i = 0; i < actualHourDifference; i++)
    {
      if (currentHour == aHour)
      {
        break;
      }
      if (aHour > currentHour)
      {
        currentHour++;
      }
      else
      {
        currentHour--;
      }
      driver.findElement(By.xpath("//android.widget.ListView[2]//android.view.View[contains(@text,\""
          + ((currentHour < 10) ? "0" : "") + currentHour + "\")]")).click();

      Thread.sleep(500);
    }
    System.out.println("Selected hour is:" + currentHour);

    // Selection of Min
    int currentMin = Integer.parseInt(driver.findElement(By.xpath("//android.widget.ListView[3]//android.view.View[@selected='true']")).getText());
    int actualMinDifference = currentMin - aMin;
    actualMinDifference = (actualMinDifference < 0) ? -actualMinDifference : actualMinDifference;
    for (int i = 0; i < (actualMinDifference / 5); i++)
    {
      if (currentMin == aMin)
      {
        break;
      }
      if (aMin > currentMin)
      {
        currentMin = currentMin + 5;
      }
      else
      {
        currentMin = currentMin - 5;
      }
      driver.findElement(By.xpath("//android.widget.ListView[3]//android.view.View[contains(@text,\""
          + ((currentMin < 10) ? "0" : "") + currentMin + "\")]")).click();
      Thread.sleep(500);
    }
    System.out.println("Selected minute is:" + currentMin);

    driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"SET\").instance(0))").click();
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aYear  DOCUMENT ME!
   * @param  aMonth DOCUMENT ME!
   * @param  aDay   DOCUMENT ME!
   * @param  aHour  DOCUMENT ME!
   * @param  aMin   DOCUMENT ME!
   *
   * @throws InterruptedException DOCUMENT ME!
   */
  @Override public void selectReservationDate(int aYear, String aMonth, int aDay, int aHour, int aMin) throws InterruptedException
  {
    driver.findElement(By.xpath("//android.widget.TextView[contains(@text,\"Create reservation from\")]")).click();
    Thread.sleep(500);
    driver.findElement(By.xpath("//android.widget.Button[contains(@text,\"Choose month and year\")]")).click();
    Thread.sleep(500);
    driver.findElement(By.xpath("//android.app.Dialog//android.view.View[contains(@text,\"" + aYear + "\")]")).click();
    Thread.sleep(500);
    driver.findElement(By.xpath("//android.app.Dialog//android.view.View[contains(@text,\"" + aMonth + "\")]")).click();
    Thread.sleep(500);
    driver.findElement(By.xpath("//android.app.Dialog//android.widget.GridView//android.view.View[contains(@text,\"" + aDay + " \")]")).click();
    Thread.sleep(500);
    driver.findElements(By.xpath("//android.widget.EditText")).get(0).clear();
    driver.findElements(By.xpath("//android.widget.EditText")).get(0).sendKeys(String.valueOf(aHour));
    Thread.sleep(500);
    driver.findElements(By.xpath("//android.widget.EditText")).get(1).clear();
    driver.findElements(By.xpath("//android.widget.EditText")).get(1).sendKeys(String.valueOf(aMin));
    Thread.sleep(500);
    driver.findElements(By.xpath("//android.app.Dialog//android.widget.Button[@index='2']")).get(1).click();
  }
}
